import {Component, OnDestroy, OnInit} from '@angular/core';
import {Sensor} from './Sensor';
import {SensorService} from './service/sensor.service';
import {Subscription} from 'rxjs';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-farm-fish-sensors',
  templateUrl: './farm-fish-sensors.component.html',
  styleUrls: ['./farm-fish-sensors.component.css']
})
export class FarmFishSensorsComponent implements OnInit, OnDestroy {
  private sensorSubscription: Subscription;
  sensors: Array<Sensor>;

  constructor(private sensorService: SensorService) {
    this.sensors = new Array<Sensor>();
  }

  private fetchSensors(): void {
    this.sensorSubscription = this.sensorService.getSensors().subscribe((resp: HttpResponse<Array<Sensor>>) => {
      resp.body.forEach((elem: Sensor) => {
        this.sensors.push(elem);
      });
    });
  }

  ngOnInit(): void {
    this.fetchSensors();
  }

  ngOnDestroy(): void {
    if (this.sensorSubscription !== undefined) {
      this.sensorSubscription.unsubscribe();
    }
  }

}
