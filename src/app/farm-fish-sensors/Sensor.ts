export interface Sensor {
  id: number;
  fishFarmId: string;
  type: string;
  value: number;
}
