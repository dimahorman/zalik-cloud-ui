export interface SensorLog {
  id: string;
  sensorId: number;
  date: number;
  updatedValue: number;
}
