import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmFishSensorsComponent } from './farm-fish-sensors.component';

describe('FarmFishSensorsComponent', () => {
  let component: FarmFishSensorsComponent;
  let fixture: ComponentFixture<FarmFishSensorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FarmFishSensorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmFishSensorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
