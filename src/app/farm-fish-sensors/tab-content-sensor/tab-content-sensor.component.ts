import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Sensor} from '../Sensor';
import {interval, Subscription} from 'rxjs';
import {SensorService} from '../service/sensor.service';
import {HttpResponse} from '@angular/common/http';
import {SensorLog} from '../SensorLog';

@Component({
  selector: 'app-tab-content-sensor',
  templateUrl: './tab-content-sensor.component.html',
  styleUrls: ['./tab-content-sensor.component.css']
})
export class TabContentSensorComponent implements OnInit, OnDestroy {
  private sensorLogsSubscription: Subscription;
  private valueSyncSubscription: Subscription;
  @Input() sensor: Sensor;
  logs: Array<SensorLog>;
  x: Array<string>;
  y: Array<number>;

  constructor(private sensorService: SensorService) {
    this.logs = new Array<SensorLog>();
    this.x = new Array<string>();
    this.y = new Array<number>();
  }

  private fetchSensorLog(): void {
    this.sensorLogsSubscription = this.sensorService.getSensorLogBySensorId(this.sensor.id)
      .subscribe((resp: HttpResponse<Array<SensorLog>>) => {
        resp.body.forEach((elem: SensorLog) => {
          this.logs.push(elem);
        });
        this.logs.sort((a, b): number => {
          if (a.date > b.date) {
            return 1;
          }
          if (a.date < b.date) {
            return -1;
          }
          return 0;
        });
        this.logs.forEach((elem: SensorLog) => {
          const date = new Date(elem.date).toUTCString();
          const value = elem.updatedValue;
          this.x.push(date);
          this.y.push(value);
        });
      });

  }

  ngOnInit(): void {
    this.fetchSensorLog();
    this.valueSyncSubscription = interval(3000).subscribe(() => {
      this.sensorService.getSensorById(this.sensor.id).subscribe((resp: HttpResponse<Sensor>) => {
        this.sensor.value = resp.body.value;
      });
    });
  }

  ngOnDestroy(): void {
    if (this.sensorLogsSubscription !== undefined) {
      this.sensorLogsSubscription.unsubscribe();
    }
    if (this.valueSyncSubscription !== undefined) {
      this.valueSyncSubscription.unsubscribe();
    }
  }

}
