import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-plot',
  templateUrl: './plot.component.html',
  styleUrls: ['./plot.component.css']
})
export class PlotComponent implements OnInit {
  @Input()
  xValues: Array<string>;

  @Input()
  yValues: Array<number>;
  public graph = {
    data: [],
    layout: {}
  };

  constructor() {
  }

  ngOnInit(): void {
    this.graph.data = [{x: this.xValues, y: this.yValues, type: 'scatter', mode: 'lines+points', marker: {color: 'red'}}];
    this.graph.layout = {autosize: true, title: 'Sensor Values'};
  }

}
