import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Sensor} from '../Sensor';
import {Observable} from 'rxjs';
import {SensorLog} from '../SensorLog';

@Injectable({
  providedIn: 'root'
})
export class SensorService {
  private readonly url = environment.url + '/sensors';

  constructor(private httpClient: HttpClient) {
  }

  public getSensors(): Observable<HttpResponse<Array<Sensor>>> {
    return this.httpClient.get<Array<Sensor>>(this.url, {observe: 'response'});
  }

  public getSensorLogBySensorId(id: number): Observable<HttpResponse<Array<SensorLog>>> {
    return this.httpClient.get<Array<SensorLog>>(this.url + '/' + id + '/logs', {observe: 'response'});
  }

  public getSensorById(id: number): Observable<HttpResponse<Sensor>> {
    return this.httpClient.get<Sensor>(this.url + '/' + id, {observe: 'response'});
  }
}
