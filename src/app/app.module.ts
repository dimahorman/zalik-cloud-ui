import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FarmFishSensorsComponent} from './farm-fish-sensors/farm-fish-sensors.component';
import {MatTabsModule} from '@angular/material/tabs';
import {TabContentSensorComponent} from './farm-fish-sensors/tab-content-sensor/tab-content-sensor.component';
import {HttpClientModule} from '@angular/common/http';
import * as PlotlyJS from 'plotly.js/dist/plotly.js';
import {PlotlyModule} from 'angular-plotly.js';
import { PlotComponent } from './farm-fish-sensors/tab-content-sensor/plot/plot.component';

PlotlyModule.plotlyjs = PlotlyJS;

@NgModule({
  declarations: [
    AppComponent,
    FarmFishSensorsComponent,
    TabContentSensorComponent,
    PlotComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatTabsModule,
    HttpClientModule,
    PlotlyModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
